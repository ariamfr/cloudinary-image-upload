const vendorsConfig = [
  {
    code: 'STR_Apple_Beats',
    file: 'AppleBeast.csv',
    workType: 'FULL_DOWNLOAD',
    cloudinaryFolder: 'eboohome/STR7/',
    downloadGallery: true,
    downloadQs: '',
    defaultExt: '.webp'
  },
  {
    code: 'SBY',
    file: 'SouthBay.csv',
    workType: 'FULL_DOWNLOAD',
    cloudinaryFolder: 'eboohome/southbay/',
    downloadGallery: true,
    downloadQs: '',
    defaultExt: '.webp'
  },
  {
    code: 'dyson',
    file: 'Dyson.csv',
    workType: 'PATH',
    cloudinaryFolder: 'eboohome/powersale/dyson/'
  },
  {
    code: 'dyson',
    file: 'Dyson.csv',
    workType: 'FULL_DOWNLOAD',
    cloudinaryFolder: 'eboohome/powersale/dyson/',
    downloadGallery: false,
    downloadQs: '',
    defaultExt: '.webp'
  },
  {
    code: 'shark',
    file: 'Shark.csv',
    workType: 'PATH',
    cloudinaryFolder: 'eboohome/powersale/shark/'
  }
];

export default vendorsConfig;
