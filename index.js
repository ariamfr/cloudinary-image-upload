import { Buffer } from 'node:buffer';
import execBuffer from 'exec-buffer';
import isCwebpReadable from 'is-cwebp-readable';
import cwebp from 'cwebp-bin';
import fs from 'fs';
import clientHTTPS from 'https';
import clientHTTP from 'http';
import { v2 as cloudinary } from 'cloudinary';
import vendors from './vendorsConfig.js';
import { read, utils, writeFile } from 'xlsx';
import { config } from 'dotenv';
import { fileURLToPath } from 'url';
import path from 'path';
import sharp from 'sharp';

config();

const MAX_WIDTH = 3000,
  MAX_HEIGHT = 3000;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const CLOUDINARY_CONFIG = {
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
};

const DEFAULT_HEADER_OPTIONS = {
  headers: {
    'User-Agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
  }
};

(async function () {
  const { default: imagemin } = await import('imagemin');
  const withoutFolders = [];

  async function parseVendor(vendor) {
    const vendorDir = `${__dirname}/vendors/${vendor.code}/`;
    console.log(`    Parse vendor in '${vendorDir}'`);
    const csvFilePath = `${vendorDir}${vendor.file}`;
    const csvProducts = csvToJson(csvFilePath);
    const metadata = [];

    async function processProducts(data) {
      if (!data.length) return;
      const product = data.pop();

      switch (vendor.workType) {
        case 'FULL_DOWNLOAD': {
          const sourceDir = `${vendorDir}source/`;
          if (!fs.existsSync(sourceDir)) fs.mkdirSync(sourceDir, { recursive: true });
          const images = [product.swatch, ...(product?.gallery_images?.split('|') || [])];

          await Promise.all(
            images.map((img) => removeFromCloudinary(`eboohome/${vendor.code}/${img.split('/').pop()}`))
          );

          await Promise.all(
            images.map((img, i) =>
              downloadImage(
                `http://${img}${vendor.downloadQs || ''}`,
                `${sourceDir}${i}${img.split('/').pop()}${vendor.defaultExt || ''}`
              )
            )
          );

          console.log(`            Optimizing images...`);
          const { cloudinaryGallery, optimized } = await optimizeImages(sourceDir, vendorDir, vendor.cloudinaryFolder);
          console.log(`            Optimizations successful done`);

          const files = fs.readdirSync(`${vendorDir}optimized`);

          const basePath = `${vendorDir}optimized/`;
          console.log(`            Uploading images to Cloudinary...`);

          let gallery = await Promise.all(
            files
              .filter((name) => !optimized.has(name))
              .map(async (name) => {
                const originalName = name.substring(1);
                const [publicId] = originalName.split('.webp');
                console.log(
                  `                Moving: '${basePath}${originalName}' to '${vendor.cloudinaryFolder}${publicId}'`
                );
                return uploadToCloudinary(`${basePath}${name}`, `${vendor.cloudinaryFolder}${publicId}`);
              })
          );

          gallery = [...cloudinaryGallery, ...gallery].sort();

          const swatch = gallery.sort().shift();
          fs.rmSync(sourceDir, { recursive: true, force: true });
          fs.rmSync(basePath, { recursive: true, force: true });
          console.log(`            Parsing 'swatch' image`);
          product.swatch = normalizeImgUrl(swatch);
          console.log(`            Parsing 'gallery_images' image`);
          product.gallery_images = normalizeImgUrl(gallery.join('|'));
          metadata.push(product);
          console.log(`        Success product '${product.sku}' processing`);
          return processProducts(data);
        }
        case 'PATH': {
          if (!fs.existsSync(`${vendorDir}${product.sku}`)) {
            console.warn(`        Dir '${vendorDir}${product.sku}' does not exist`);
            withoutFolders.push(`${vendor.code}->${product.sku}`);
            product.gallery_images = '';
            metadata.push(product);
            return processProducts(data);
          }

          console.log(`        Processing '${product.sku}'`);
          if (product.swatch) {
            const [targetMainName, ext] = product.swatch.split('/').pop().split('.');
            const targetMainFullName = `${targetMainName}.${ext}`;
            const targetMainDir = `${vendorDir}${product.sku}/${targetMainFullName}`;
            if (!fs.existsSync(targetMainDir)) {
              console.log(`            Download 'swatch' '${product.swatch}'`);
              await downloadImage(`http://${product.swatch}`, targetMainDir);
              console.log(`            Swatch success downloaded`);
            }
          }
          /**
           * @description Optimize all images to webp to reduce bandwidth
           */
          console.log(`            Optimizing images...`);
          const { cloudinaryGallery, optimized } = await optimizeImages(
            `${vendorDir}${product.sku}/`,
            undefined,
            vendor.cloudinaryFolder,
            false
          );
          console.log(`            Optimizations successful done`);

          const files = fs.readdirSync(`${vendorDir}${product.sku}/optimized`);

          /**
           * @description Upload all optimized images to cloudinary
           */
          const basePath = `${vendorDir}${product.sku}/optimized/`;
          console.log(`            Uploading images to Cloudinary...`);
          const response = await Promise.all(
            files
              .filter((name) => !optimized.has(name))
              .map(async (name) => {
                let [publicId] = name.split('.webp');
                console.log(`                Moving: '${basePath}${name}' to '${vendor.cloudinaryFolder}${publicId}'`);
                return uploadToCloudinary(`${basePath}${name}`, `${vendor.cloudinaryFolder}${publicId}`);
              })
          );

          const { images, videos } = response.reduce(
            (data, url) => {
              data[/\.mp4$/.test(url) ? 'videos' : 'images'].push(url);
              return data;
            },
            { images: [], videos: [] }
          );

          const [swatch, ...gallery] = [...images, ...cloudinaryGallery].sort();
          console.log(`            Parsing 'swatch' image`);
          product.swatch = normalizeImgUrl(swatch);
          console.log(`            Parsing 'gallery_images' image`);
          product.gallery_images = normalizeImgUrl(gallery.join('|'));
          console.log(`            Parsing 'video_url' video`);
          product.video_url = normalizeVideoUrl(
            product.video_url ? `${product.video_url}|${videos.join('|')}` : videos.join('|')
          );
          metadata.push(product);
          console.log(`        Success product '${product.sku}' processing`);
          return processProducts(data);
        }
      }
    }

    console.log(`    Start products image processing...`);
    await processProducts(csvProducts);
    console.log(`    End products image processing...`);
    console.log(`    Updating source '${vendor.file}'...`);
    jsonToCsv(metadata, csvFilePath);
    console.log(`    Source '${vendor.file}' successful updated`);
  }

  async function processVendors(v) {
    if (!v.length) return;
    const vendor = v.pop();
    console.log(`Processing vendor '${vendor.code.toUpperCase()}'`);
    await parseVendor(vendor);
    console.log(`Success vendor '${vendor.code.toUpperCase()}' processed
  ********************************************************************`);
    await processVendors(v);
  }

  processVendors(vendors).then(() =>
    console.log('FINISHED: \n', `Without gallery: ${JSON.stringify(withoutFolders, null, 2)}`)
  );

  // region UTILS
  function jsonToCsv(products, target) {
    const wb = utils.book_new();
    wb.SheetNames.push('import');
    wb.Sheets['import'] = utils.json_to_sheet(products);
    writeFile(wb, target);
  }

  function csvToJson(localFilePath) {
    const data = fs.readFileSync(localFilePath);
    const workbook = read(data, { type: 'buffer', FS: ',' });
    const sheet = workbook.Sheets[workbook.SheetNames[0]];
    return utils.sheet_to_json(sheet, { raw: true });
  }

  function addFileExtension(res, filepath) {
    if (!/\.(jpg|jpeg|png|webp|tif)$/.test(filepath)) {
      let contentType = res.headers['content-type'];
      const ext = contentType.split('/').pop();
      filepath += `.${ext}`;
    }
    return filepath;
  }

  function getImage(url, resolve, reject, filepath, client = clientHTTP) {
    client.get(url, DEFAULT_HEADER_OPTIONS, (res) => {
      if (res.statusCode === 200) {
        filepath = addFileExtension(res, filepath);
        res
          .pipe(fs.createWriteStream(filepath, {}))
          .on('error', reject)
          .once('close', () => resolve(filepath));
      } else {
        res.resume();

        if (/^http:\/\//.test(url)) {
          getImage(url.replace('http', 'https'), resolve, reject, filepath, clientHTTPS);
        } else {
          resolve(`https://error.getting.image`);
          console.log(`Request Failed With a Status Code: ${res.statusCode} '${url}'`);
        }
        // } else reject(`Request Failed With a Status Code: ${res.statusCode} '${url}'`);
      }
    });
  }

  function downloadImage(url, filepath) {
    filepath = filepath.replace(/[?=&]/gi, '_').split('.');
    filepath.pop();
    filepath = filepath.join('.');

    return new Promise((resolve, reject) => {
      getImage(url, resolve, reject, filepath);
    });
  }

  // region UPLOAD TO CLOUDINARY
  async function uploadToCloudinary(source, public_id, options = {}) {
    cloudinary.config(CLOUDINARY_CONFIG);
    public_id = public_id.trim();
    try {
      const url = await new Promise((resolve, reject) => {
        cloudinary.uploader.upload_large(
          source,
          { public_id, resource_type: 'auto', ...options },
          function (error, result) {
            if (error) {
              console.log(error);
              return reject('');
            }
            resolve(result.url);
          }
        );
      });

      console.log('                UPLOAD RESPONSE: ', url);
      return url || '';
    } catch (e) {
      console.log(e);
    }
  }

  async function removeFromCloudinary(public_id) {
    cloudinary.config(CLOUDINARY_CONFIG);
    try {
      console.log('                DESTROYING SOURCE: ', public_id);
      const response = await cloudinary.uploader.destroy(public_id, { invalidate: true, resource_type: 'image' });
      console.log('                DESTROY RESPONSE: ', response);
      return response;
    } catch (e) {
      console.log(e);
    }

    return {};
  }

  // endregion

  async function optimizeImages(dir, destination = dir, cloudinaryFolder = '', orderName = true) {
    const cloudinaryGallery = [];
    const optimized = new Set();
    !fs.existsSync(`${destination}/optimized`) ? fs.mkdirSync(`${destination}/optimized`) : undefined;
    try {
      await imagemin([`${dir}*.{jpg,jpeg,png,webp,tif,mp4}`], {
        destination: `${destination}/optimized`,
        plugins: [imageminWebp({ quality: 75, method: 6, lossless: 9 })]
      });

      const files = fs.readdirSync(`${destination}/optimized`).filter((name) => !/\.webp$/.test(name));

      if (files.length) {
        console.log('                NEED OPTIMIZATION THROUGH CLOUDINARY: ', files.join(' | '));

        const sourceBasePath = `${destination}optimized/`;
        await Promise.allSettled(
          files.map(async (name) => {
            const originalName = orderName ? name.substring(1) : name;
            const [publicId] = originalName.split(`.${/[^.]+$/.exec(name)[0]}`);
            console.log(
              `                    Moving direct to cloudinary: '${sourceBasePath}${originalName}' to '${cloudinaryFolder}${publicId}'`
            );
            const url = await uploadToCloudinary(`${sourceBasePath}${name}`, `${cloudinaryFolder}${publicId}`, {
              format: 'webp'
            });
            cloudinaryGallery.push(url);
            optimized.add(name);
          })
        );
      }
    } catch (e) {
      console.log('ERROR.OPTIMIZATION: ', e.message);
    }
    return { cloudinaryGallery, optimized };
  }

  function normalizeImgUrl(url) {
    return url.replace(/https?:\/\//gi, '').replace(/\/v\d{10}\//gi, '/v1/');
  }

  function normalizeVideoUrl(url) {
    return url.replace(/https?:\/\//gi, 'https://');
  }

  // endregion
})();

const imageminWebp =
  (options = {}) =>
  async (input) => {
    if (!Buffer.isBuffer(input)) {
      return Promise.reject(
        new TypeError(`Expected \`input\` to be of type \`Buffer\` but received type \`${typeof input}\``)
      );
    }

    if (!isCwebpReadable(input)) {
      return Promise.resolve(input);
    }

    const args = ['-quiet', '-mt'];

    if (options.preset) {
      args.push('-preset', options.preset);
    }

    if (options.quality) {
      args.push('-q', options.quality);
    }

    if (options.alphaQuality) {
      args.push('-alpha_q', options.alphaQuality);
    }

    if (options.method) {
      args.push('-m', options.method);
    }

    if (options.size > 0) {
      args.push('-size', options.size);
    }

    if (options.sns) {
      args.push('-sns', options.sns);
    }

    if (options.filter) {
      args.push('-f', options.filter);
    }

    if (options.autoFilter) {
      args.push('-af');
    }

    if (options.sharpness) {
      args.push('-sharpness', options.sharpness);
    }

    if (options.lossless) {
      if (typeof options.lossless === 'number') {
        args.push('-z', options.lossless);
      } else {
        args.push('-lossless');
      }
    }

    if (options.nearLossless) {
      args.push('-near_lossless', options.nearLossless);
    }

    if (options.crop) {
      args.push('-crop', options.crop.x, options.crop.y, options.crop.width, options.crop.height);
    }

    if (options.resize) {
      args.push('-resize', options.resize.width, options.resize.height);
    } else if (!options.resize) {
      const image = await sharp(input);
      let { width: w, height: h } = await image.metadata();

      // decide the width
      if (MAX_WIDTH < w || MAX_HEIGHT < h) {
        if (w > MAX_WIDTH) {
          h = (MAX_WIDTH / w) * h;
          w = MAX_WIDTH;
        }
        // decide the height
        if (h > MAX_HEIGHT) {
          w = (MAX_HEIGHT / h) * w;
          h = MAX_HEIGHT;
        }
        args.push('-resize', String(w), String(h));
      }
    }

    if (options.metadata) {
      args.push('-metadata', Array.isArray(options.metadata) ? options.metadata.join(',') : options.metadata);
    }

    args.push('-o', execBuffer.output, execBuffer.input);

    return execBuffer({
      args,
      bin: cwebp,
      input
    }).catch((error) => {
      error.message = error.stderr || error.message;
      // console.log('              ERROR.IMAGEMIN_WEBP: ', JSON.stringify({ ...error }, null, 2));
      return input;
    });
  };
